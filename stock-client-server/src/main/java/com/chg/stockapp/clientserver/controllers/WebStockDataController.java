package com.chg.stockapp.clientserver.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.chg.stockapp.clientserver.services.StockDataProcessorService;
import com.chg.stockapp.common.model.MarketDataRequest;
import com.chg.stockapp.common.model.StockDataDto;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class WebStockDataController {
    private final RSocketRequester rSocketRequester;
    private StockDataProcessorService stockDataProcessorService;

    
    public WebStockDataController(RSocketRequester rSocketRequester,StockDataProcessorService stockDataProcessorService) {
        this.rSocketRequester = rSocketRequester;
        this.stockDataProcessorService = stockDataProcessorService;
    }
    
	@GetMapping("/get-stock-data/{type}") 
	public Mono<String> getStockData(@PathVariable("type") String type) {
		System.out.println("*** Get Data... " + type);
        Mono<MarketDataRequest> response = rSocketRequester
          .route("loadStockData")
          .data(new MarketDataRequest(type))
          .retrieveMono(MarketDataRequest.class);
        response.subscribe(r -> {System.out.println("Response: " + r.getStock());});
        return Mono.just("Sucess - " + type);
	}
	
	@GetMapping("/count-stock-data/{type}") 
	public Mono<Long> countStockData(@PathVariable("type") String type) {
		System.out.println("*** Count Data... " + type);
        Mono<Long> response = rSocketRequester
          .route("countStockData")
          .data(new MarketDataRequest(type))
          .retrieveMono(Long.class);
        return response;
	}
	
	@GetMapping("/all-stock-data/{type}")
	public Flux<StockDataDto> allStockData(@PathVariable("type") String type) {
		System.out.println("*** All Data... " + type);
		Flux<StockDataDto> response = rSocketRequester
          .route("allStockData")
          .data(new MarketDataRequest(type))
          .retrieveFlux(StockDataDto.class)
          .filter(stockData -> stockData.getName().startsWith(type))
          .take(1000);
        
		return response;
	}
	
	@GetMapping("/search-stock-data/{search}")
	public Flux<StockDataDto> searchStockData(@PathVariable("search") String search) {
		System.out.println("*** Search Data... " + search);
		Flux<StockDataDto> response = rSocketRequester
          .route("searchStockData")
          .data(new MarketDataRequest(search))
          .retrieveFlux(StockDataDto.class);
        
		return response;
	}
	
	@GetMapping("/process-stock-data/{search}")
	public Mono<String> processStockData(@PathVariable("search") String search) {
		System.out.println("*** Search Data... " + search);
		Flux<StockDataDto> response = rSocketRequester
          .route("searchStockData")
          .data(new MarketDataRequest(search))
          .retrieveFlux(StockDataDto.class);
		
		stockDataProcessorService.processStockData(response);
        
		return Mono.just("Sucess");
	}
	
	@GetMapping("/test-web-client/{search}")
	public Flux<StockDataDto> testWebClient(@PathVariable("search") String search) {
		System.out.println("*** Web Client... " + search);
		
		WebClient client = WebClient
				  .builder()
				  .baseUrl("http://localhost:8080")
				  .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)   
				  .build();
		
		Flux<StockDataDto> stockData = client
				.get()
				.uri("/search-stock-data/{search}",search)
				.retrieve().bodyToFlux(StockDataDto.class).log();
		
//		Mono<List<StockDataDto>> stockData = 
//				client
//				.get()
//				.uri("/search-stock-data/{search}",search)
//				.retrieve().bodyToFlux(StockDataDto.class).collectList();
		
		
		return stockData;
	}
	
	
}
