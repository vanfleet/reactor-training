package com.chg.stockapp.clientserver.services;

import org.reactivestreams.Subscription;
import org.springframework.stereotype.Service;

import com.chg.stockapp.common.model.StockDataDto;

import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;

@Service
public class StockDataProcessorService {

	
	public void processStockData(Flux<StockDataDto> stockData) {
		System.out.println("Subscribing to stockData...");
		stockData.subscribe(new StockDataSubscriber(25,200));
	}
	
	public void processStockData(StockDataDto stockData) {
		System.out.println("  Processing: " + stockData.getName());
	}
	
	
	
	/*
	 * Custom Subscriber
	 */
	public class StockDataSubscriber extends BaseSubscriber<StockDataDto> {
		private int count=0;		
		private int size;
		private int stopAt;
		
		public StockDataSubscriber(int size, int stopAt) {
			this.size=size;
			this.stopAt=stopAt;
		}

		@Override
		protected void hookOnSubscribe(Subscription subscription) {
			System.out.println("*** onSubscribe, request: " + size);
	        request(size);
		}

		@Override
		protected void hookOnNext(StockDataDto stockData) {
			count++;
			
			if (count==stopAt) {
				System.out.println("*** Cancel at: " + count);
				cancel();
			}
	        
	        if (count % size == 0) {
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
	        	System.out.println("*** Requesing: " + size);
	        	request(size);
	        }
	        
	        processStockData(stockData);
			
		}

		@Override
		protected void hookOnComplete() {
			System.out.println("*** Done processing " + count + " stocks.");
		}

		@Override
		protected void hookOnError(Throwable throwable) {
			throwable.printStackTrace();
		}

	}
}
