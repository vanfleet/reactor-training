package com.chg.stockapp.clientserver.testArea;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

import org.reactivestreams.Subscription;

import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;

public class ReactiveExamples {

	public static void main(String[] args) {
		ReactiveExamples reactiveTest = new ReactiveExamples();
		//reactiveTest.fluxTest();
		//reactiveTest.exampleOne();
		//reactiveTest.backPressureExample();
		//reactiveTest.flatMapExample();
		//reactiveTest.mapExample();
		//reactiveTest.zipWithExample();
		//reactiveTest.filterExample();
		//reactiveTest.generateExample();
		reactiveTest.generateExample2();

	}

	void fluxTest() {
		// Create Flux publisher
		// Flux publisher contains a sequence of 0-n items
		// Operations can then be performed on the sequence using various operators like map or take.
		Flux<String> ints = Flux.range(1, 100)
				.map(i -> {
					//if (i<=5)
						return i + "";
					//throw new RuntimeException("Got to 6");
				}).take(80).limitRate(20).log();

		// once the publisher with its operators are defined, then we subscribe to it.
		// Then the operators are executed.
		ints.subscribe(i -> {
				System.out.println("int: " + i);
			}, err -> {
				System.err.println("Error: " + err);
			}, () -> {
				System.out.println("Done");
			},sub -> {
				sub.request(5);
			});

		//ints.subscribe(new MySubscriber());

	}

	void exampleOne() {
//		Flux<String> ints = Flux.range(1,100)
//			.map(i -> {
//				if (i==13) throw new RuntimeException("i is 13");
//				return String.valueOf(i);
//			}); //.onErrorResume(e-> Flux.just("foo", "bar", "foobar")).take(30).log();
//
//		ints.subscribe(new MySubscriber());
		
		// flatMap
		Flux<Integer> integers = Flux.range(0, 1000)
				.flatMap(i -> {
					if (i==13) throw new RuntimeException("got to 13");
					return Flux.just(i,i);
				}).onErrorReturn(33).take(100).log();
		
		integers.subscribe(); //new MySubscriber<Integer>());

	}
	
	/*
	 * Backpressure Example using custom subscriber, request 10 at a time
	 */
	void backPressureExample() {
		Flux<String> items = Flux.range(1, 100).map(i->String.valueOf(i)).log();
		items.subscribe(new MySubscriber(10,-1));
	}
	
	/* FlatMap Example
	 * For each item in the Flux pass back a Mono or Flux.
	 * If you have a flux of arrays those arrays could be flattened into one
	 */
	void flatMapExample() {
		
		Flux<Integer> lists = Flux.just(Arrays.asList(1,2,3),Arrays.asList(4,5,6),Arrays.asList(7,8,9))
				.flatMap(list -> {
					return Flux.fromStream(list.stream());
				});
		
		lists.subscribe(System.out::println);
	}
	
	/*
	 * ZipWith Example
	 * zip two publishers together into one publisher
	 */
	void zipWithExample() {
		Flux<String> strings = Flux.just("one","two","three","four");
		Flux<Integer> integers = Flux.range(1, 4);
		
		Flux<String> combined = strings.zipWith(integers, (s,i) -> s.concat(" - " + i));
		combined.subscribe(System.out::println);

	}
	/*
	 * Filter Example
	 * Filter a publisher resulting in a publishere with filters data
	 */
	void filterExample() {
		Flux<String> filter = Flux.just("one","two","three","four")
				.filter(s -> s.startsWith("t"));
		filter.subscribe(System.out::println);
	}
	
	
	/*
	 * map - go through each item in the flux and return it (modified, for example change 
	 * the integer into a string).
	 */
	void mapExample() {
		Flux<String> integers = Flux.range(0, 1000)
		.map(i -> {
			return String.valueOf(i);
		}).take(100).log();
		
		integers.subscribe(System.out::println);
	}
	
	/*
	 * Programmatically create a publisher
	 * sink - API to trigger events, onNext, onError, onComplete
	 */
	void generateExample() {
		Flux<Object> strings = Flux.generate(
			() -> 0, 
			(state,sink) -> {
				sink.next("item " + state);
				if (state == 20) sink.complete();
				return state+1;
		}).log();
		
		strings.subscribe(System.out::println);
	}
	
	void generateExample2() {
		Flux<Object> strings = Flux.generate(
			AtomicLong::new, 
			(state,sink) -> {
				long l = state.getAndIncrement();
				sink.next("item " + l);
				if (l == 20) sink.complete();
				return state;
		}).log();
		
		strings.subscribe(System.out::println);
	}
	
	
	// ----------------------------------------------------------------
	
	
	/*
	 * Custom Subscriber
	 */
    public class MySubscriber extends BaseSubscriber<String> {
        private int count=0;
        private int size;
        private int stopAt;

        public MySubscriber(int size, int stopAt) {
            this.size=size;
            this.stopAt=stopAt;
        }

        @Override
        protected void hookOnSubscribe(Subscription subscription) {
            System.out.println("*** onSubscribe, request: " + size);
            request(size);
        }

        @Override
        protected void hookOnNext(String str) {
            count++;

            if (count==stopAt) {
                System.out.println("*** Cancel at: " + count);
                cancel();
            }

            if (count % size == 0) {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("*** Requesing: " + size);
                request(size);
            }

        }

        @Override
        protected void hookOnComplete() {
            System.out.println("*** Done processing " + count + " items.");
        }

        @Override
        protected void hookOnError(Throwable throwable) {
            throwable.printStackTrace();
        }

    }

}
