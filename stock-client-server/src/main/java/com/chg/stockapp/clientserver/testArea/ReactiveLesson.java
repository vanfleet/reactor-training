package com.chg.stockapp.clientserver.testArea;

import org.reactivestreams.Subscription;

import reactor.core.publisher.BaseSubscriber;

public class ReactiveLesson {

	public static void main(String[] args) {
		ReactiveLesson reactiveExamples=new ReactiveLesson();
		reactiveExamples.exampleOne();
	}
	
	void exampleOne() {
		
	}
	
	
	
	
	/*
	 * Custom Subscriber
	 */
    public class MySubscriber extends BaseSubscriber<String> {
        private int count=0;
        private int size;
        private int stopAt;

        public MySubscriber(int size, int stopAt) {
            this.size=size;
            this.stopAt=stopAt;
        }

        @Override
        protected void hookOnSubscribe(Subscription subscription) {
            System.out.println("*** onSubscribe, request: " + size);
            request(size);
        }

        @Override
        protected void hookOnNext(String str) {
            count++;

            if (count==stopAt) {
                System.out.println("*** Cancel at: " + count);
                cancel();
            }

            if (count % size == 0) {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("*** Requesing: " + size);
                request(size);
            }

        }

        @Override
        protected void hookOnComplete() {
            System.out.println("*** Done processing " + count + " items.");
        }

        @Override
        protected void hookOnError(Throwable throwable) {
            throwable.printStackTrace();
        }

    }

}
