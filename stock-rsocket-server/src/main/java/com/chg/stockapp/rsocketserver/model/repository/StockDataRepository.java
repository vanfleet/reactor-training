package com.chg.stockapp.rsocketserver.model.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.chg.stockapp.rsocketserver.model.db.StockData;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface StockDataRepository extends ReactiveCrudRepository<StockData, String> {
	Mono<StockData> findByName(Mono<String> name);
	Flux<StockData> findByNameContains(Mono<String> search);
}
