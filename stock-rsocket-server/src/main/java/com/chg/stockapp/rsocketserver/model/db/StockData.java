package com.chg.stockapp.rsocketserver.model.db;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class StockData {
	private @Id String id;
	private String name;
	private String ticker;
    private double currentPrice;
    
    public StockData() {}
    		
	public StockData(String name, String ticker, double currentPrice) {
		super();
		this.name = name;
		this.ticker = ticker;
		this.currentPrice = currentPrice;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	public double getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}
	
	@Override
	public String toString() {
		return "StockData [id=" + id + ", name=" + name + ", ticker=" + ticker + ", currentPrice=" + currentPrice + "]";
	}
    
    
}
