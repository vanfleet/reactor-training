package com.chg.stockapp.rsocketserver.services;

import java.util.Random;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.chg.stockapp.common.model.StockDataDto;
import com.chg.stockapp.rsocketserver.model.db.StockData;
import com.chg.stockapp.rsocketserver.model.repository.StockDataRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class StockDataService {
	private StockDataRepository stockDataRepository;
	private Random random = new Random();
	private ModelMapper modelMapper = new ModelMapper();
	
	public StockDataService(StockDataRepository stockDataRepository) {
		this.stockDataRepository=stockDataRepository;
	}

	@PostConstruct
	public void initService() {
		System.out.println("Stock Data Repository cleanup");
		Mono<Long> count = stockDataRepository.count().log();
		Mono<Void> deleteAll = stockDataRepository.deleteAll().log();
		
		//Uncomment the next line if you want to remove all the data in the database.
		//Flux.concat(count,deleteAll).subscribe(System.out::println);
	}
	

	
	public Mono<Long> countStockData() {
		return stockDataRepository.count();
	}
	
	/*
	 * Return all the stock data
	 */
	public Flux<StockDataDto> getAllStockData() {
		return stockDataRepository.findAll()
			.map(stockData -> 
				modelMapper.map(stockData, StockDataDto.class)
		);
	}
	
	/*
	 * Search the database by the given search string, return all the matches.
	 */
	public Flux<StockDataDto> searchStockData(String search) {
		return stockDataRepository.findByNameContains(Mono.just(search))
			.map(stockData -> 
				modelMapper.map(stockData, StockDataDto.class)
			).log();
	}
	
	/*
	 * Search the database by the given search string, return all the matches.
	 */
	public Mono<StockDataDto> findStockData(String name) {
		return stockDataRepository.findByName(Mono.just(name))
			.map(stockData -> 
				modelMapper.map(stockData, StockDataDto.class)
			).log();
	}
	
	
	
	/*
	 * Generate and save some stock data.
	 */
	public void loadStockData() {
		System.out.println("Loading Stock Data...");
		Flux<StockData> stockData = getStockData();
		stockDataRepository.saveAll(stockData).subscribe();
	}
	
	/*
	 * Randomly generate 20000 stock items, and return them as a Flux.
	 */
    private Flux<StockData> getStockData() {
    	Flux<StockData> marketDataList = Flux.range(1,20000).map(i -> {
    		return RandomStringUtils.random(5, true, false);
    	}).distinct().map(t -> {
    		return new StockData(t, t, random.nextDouble());
    	});
    	
    	return marketDataList;
    }
    
    
}
