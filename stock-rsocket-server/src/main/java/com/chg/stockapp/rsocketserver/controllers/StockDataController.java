package com.chg.stockapp.rsocketserver.controllers;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import com.chg.stockapp.common.model.MarketDataRequest;
import com.chg.stockapp.common.model.StockDataDto;
import com.chg.stockapp.rsocketserver.services.StockDataService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
public class StockDataController {
	private StockDataService stockDataService;
	
	public StockDataController(StockDataService stockDataService) {
		this.stockDataService=stockDataService;
	}
	
    @MessageMapping("loadStockData")
    public Mono<MarketDataRequest> loadStockData(MarketDataRequest type) {
    	System.out.println("RSocket loadStockData mapping: " + type);
        stockDataService.loadStockData();
        return Mono.just(type);
    }
    
    @MessageMapping("countStockData")
    public Mono<Long> countStockData(MarketDataRequest type) {
    	System.out.println("RSocket count loadStockData mapping: " + type);
        return stockDataService.countStockData();
    }
    
    @MessageMapping("allStockData")
    public Flux<StockDataDto> allStockData(MarketDataRequest type) {
    	System.out.println("RSocket get all StockData mapping: " + type);
        return stockDataService.getAllStockData();
    }
    
    @MessageMapping("searchStockData")
    public Flux<StockDataDto> searchStockData(MarketDataRequest type) {
    	System.out.println("RSocket search StockData mapping: " + type);
        return stockDataService.searchStockData(type.getStock());
    }
}
