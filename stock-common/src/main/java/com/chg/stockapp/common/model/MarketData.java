package com.chg.stockapp.common.model;

public class MarketData {
	static long counter=1;
	long id=0;
    private String stock;
    private int currentPrice;
    
    public MarketData() {
    }
    public MarketData(String stock, int currentPrice) {
		super();
		this.id = counter++;
		this.stock = stock;
		this.currentPrice = currentPrice;
	}
    

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStock() {
		return stock;
	}


	public void setStock(String stock) {
		this.stock = stock;
	}

	public int getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(int currentPrice) {
		this.currentPrice = currentPrice;
	}


	public static MarketData fromException(Exception e) {
        MarketData marketData = new MarketData();
        marketData.setStock(e.getMessage());
        return marketData;
    }
	@Override
	public String toString() {
		return "MarketData [id=" + id + ", stock=" + stock + ", currentPrice=" + currentPrice + "]";
	}
	
	
}
