package com.chg.stockapp.common.model;

public class MarketDataRequest {
	private String stock;
	
	public MarketDataRequest() {
		super();
	}

	public MarketDataRequest(String stock) {
		super();
		this.stock = stock;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}
	
}
