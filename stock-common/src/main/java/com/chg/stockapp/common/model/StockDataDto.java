package com.chg.stockapp.common.model;

public class StockDataDto {
	
	private String id;
	private String name;
	private String ticker;
    private double currentPrice;
    
    public StockDataDto() {
    }
    public StockDataDto(String ticker, String name, double currentPrice) {
		super();
		
		this.ticker = ticker;
		this.name=name;
		this.currentPrice = currentPrice;
	}


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	public double getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}
	public static StockDataDto fromException(Exception e) {
        StockDataDto marketData = new StockDataDto();
        marketData.setTicker(e.getMessage());
        return marketData;
    }
	
	@Override
	public String toString() {
		return "StockDataDto [id=" + id + ", name=" + name + ", ticker=" + ticker + ", currentPrice=" + currentPrice
				+ "]";
	}
	

}
