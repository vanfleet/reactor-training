#Reactor Example Project

This is a simple reactive stock application used to generate and search stock 
data, with ticker symbols, stock names, and prices all randomly generated 
This project contains the following modules:

* stock-rsocket-server - This server starts up an rsocket server and it connects to a 
Mongo database where stock information is stored.
* stock-client-server - This server starts a webflux server (netty) and also connects to the 
rsocket server.
* stock-common is a common library that contains common model objects used by the other two projects.

##Startup instructions:

* The stock-rsocket-server service requires a mongo database running on port 27017, starting this server can easily be 
accomplished by installing it as a docker container. To to install and run mongo use the following command.

    docker run --name mongo -d -p 27017:27017 mongo
    
* Start the stock-rsocket-server spring boot application from within your IDE.
* Start the stock-client-server Spring boot application from within your IDE.

## Running the Stock application

* First verify that the services are up and running try the following URL from your browser. Make sure you get a response back.

    http://localhost:8080/count-stock-data/

* The following endpoints can also be used;
    * _http://localhost:8080/get-stock-data/aa_ - Generate 20,000 stocks. 
    You can run this multiple times to generate more data. 
    * _http://localhost:8080/count-stock-data/aa_ - count the number of stocks stored in the database and return the number.
    * _http://localhost:8080/search-stock-data/{search} - return all the stocks that match the search string.
    * _http://localhost:8080/process-stock-data/{search} - search for and simulate some internal processing on the stocks. No real work is done here, but it shows how backpressure works in the reactive system.
